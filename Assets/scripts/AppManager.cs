﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;

public class AppManager : EngineObject {

  public InputParser ip;
  UiDialog uiDiag;

  UnityWebRequest queryUpload;

  protected override void setupEarly()
  {
    base.setupEarly();

    ip = qh.gc<InputParser>();
    ip.onParsingFinished += onParsing;
  }

  protected override void setup()
  {
    base.setup();

    uiDiag = qh.gc<UiDialog>();
    uiDiag.onInput += onInput;
  }
  
  public void checkSendOnline()
  {

    // -- send online

    string path = InputParser.getSortListFilePath();

    string fullInputs = File.ReadAllText(path);

    queryUpload = WebBridge.get().query("http://www.andreberlemont.com/db/query.php",
      delegate (string output)
      {
        Debug.Log("query completed " + Time.time);
        Debug.Log(output);

        File.WriteAllText(path, ""); // empty local file
        Debug.Log("reseted inputs sort file after query was successful");

        qh.gc<ScTools>().onLocalContent();
      },
      "id", "tchat_sort", "act", "append", "ct", fullInputs);

  }

  public bool isUploading()
  {
    if (queryUpload == null) return false;
    return !queryUpload.isDone;
  }

  public void onInput(string val)
  {
    if (ip == null) return;
    ip.parse(val);
  }

  public void onParsing(string answer)
  {
    //StopAllCoroutines();
    StartCoroutine(processAnswered(answer));
  }

  IEnumerator processAnswered(string answer)
  {

    IEnumerator time = HalperCoroutine.processTimer(Random.Range(0.5f, 1.5f));
    while (time.MoveNext()) yield return null;

    uiDiag.injectDialog(answer, false);

  }

  public bool isBotThinking()
  {
    return false;
  }


  private void OnDestroy()
  {
    ip.destroy(); // release file stream
    ip = null;
  }
}
