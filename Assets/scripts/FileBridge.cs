﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
public class FileBridge
{
  static protected List<TextAsset> bufferTextAssets;

  static public void logDictionary(Dictionary<string, string[]> dico, string title)
  {
    string debugOutput = "vvv "+title+" vvv";

    foreach (KeyValuePair<string, string[]> kp in dico)
    {
      debugOutput += "\n" + kp.Key;
      foreach(string line in kp.Value)
      {
        debugOutput += "\n  " + line;
      }
    }

    Debug.Log(debugOutput);
  }
}