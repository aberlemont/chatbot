﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Random = UnityEngine.Random;
using Object = UnityEngine.Object;

/// <summary>
/// https://www.npmjs.com/package/word-list-json
/// 
/// input string
/// 
/// 1) receive input : "word word word" (I love saussages)
/// 2) gather data about input : Phrase() : [moi][aimer][viande]
/// 3) search for matching pattern
/// 4) select best suited pattern
/// 5) select answer of selected pattern
/// 6) return answer
/// 
/// </summary>

public class InputParser : MonoBehaviour
{
  public Action<string> onParsingFinished;

  //https://docs.microsoft.com/en-us/dotnet/api/system.io.streamwriter?view=netframework-4.7.2
  //StreamWriter _sort;

  const string prefix_concept = "concept_";

  // loaded data (from text files)
  public Dictionary<string, string[]> _concepts; 
  public List<Pattern> _patterns;
  
  public enum ParserState { IDLE, THINKING };
  public ParserState state = ParserState.IDLE;

  private void Awake()
  {
    _concepts = HalperUnity.loadResourcesLines("", prefix_concept);
    FileBridge.logDictionary(_concepts, "concepts");

    loadPatterns();

    //_sort = HalperIO.getStream(getSortListFilePath());
  }
  
  /// <summary>
  /// les patterns dans le fichier _rules
  /// </summary>
  protected void loadPatterns()
  {
    _patterns = new List<Pattern>();

    //Dictionary<string, string[]> list = FileBridge.resourcesTxtaWithPrefix("pattern");

    KeyValuePair<string, string[]> rules = HalperUnity.loadResourceLine("", "rules");
    
    string answer = "";
    Pattern pat = null;

    for (int i = 0; i < rules.Value.Length; i++)
    {
      string line = rules.Value[i];
      if (line[0] == '(') // new pattern begins
      {
        if(pat != null)
        {
          _patterns.Add(pat);
          pat = null;
        }

        pat = new Pattern(line);
        pat.answers = new List<string>();
      }
      else
      {
        if (line[0] == '-')
        {
          if(answer.Length > 0)
          {
            pat.answers.Add(answer);
          }

          line = line.Substring(1, line.Length - 1); // remove - at start
          answer = line;
        }
        else
        {
          answer += line;
        }
      }
    }

    _patterns.Add(pat);

    Debug.Log("  loaded " + _patterns.Count + " patterns");
  }
  
  public void destroy()
  {
    //_sort.Close();
    //Debug.Log("streams closed");
  }


  /// <summary>
  /// entry point to get an answer
  /// </summary>
  /// <param name="input"></param>
  public void parse(string input)
  {
    Debug.Log("parser received : <b>" + input + "</b>");

    Phrase pw = new Phrase(input, this);

    StartCoroutine(processPhrase(pw, 
      delegate (string answer) {
        if (answer.Length <= 0)
        {
          Debug.LogWarning("answered nothing");
          answer = "..?";

          saveInput(input);
        }

        state = ParserState.IDLE;

        if (onParsingFinished != null) onParsingFinished(answer);
      })
    );

  }

  protected void saveInput(string input)
  {

    //string _sort = File.ReadAllText(getSortListFilePath());
    //File.WriteAllText(getSortListFilePath(), input);
    //StreamWriter sw = new StreamWriter(getSortListFilePath());
    //sw.WriteLine(input);
    //sw.Close();

    string path = getSortListFilePath();

    // -- save in local file

    string line = Environment.NewLine + PprefsManager.uniq+"|"+HalperNatives.getFullDate() + "=" + input;
    File.AppendAllText(path, line);
    
    Debug.Log("saved input to sort file "+line);

    qh.gc<ScTools>().onLocalContent();
  }

  public bool hasLocalContent()
  {
    string path = getSortListFilePath();
    return File.ReadAllText(path).Length > 0;
  }

  IEnumerator processPhrase(Phrase phrase, Action<string> onComplete)
  {
    state = ParserState.THINKING;

    yield return null;

    Pattern pat = phrase.getBestPattern();

    string answer = "";

    if (pat != null)
    {
      answer = pat.getRandomAnswer();
    }

    onComplete(answer);
  }

  public List<Pattern> getMatchingPatterns(Phrase phrase)
  {
    List<Pattern> list = new List<Pattern>();
    foreach (Pattern pat in _patterns)
    {
      if (pat.isPhraseMatching(phrase)) list.Add(pat);
    }
    return list;
  }

  /// <summary>
  /// if word is part of a concept's list
  /// </summary>
  /// <param name="word"></param>
  /// <returns></returns>
  public List<string> getMatchingConcepts(string word)
  {
    List<string> concepts = new List<string>();
    foreach (KeyValuePair<string, string[]> kp in _concepts)
    {
      for (int j = 0; j < kp.Value.Length; j++)
      {
        if (kp.Value[j] == word) {
          //remove 'concept_' name
          concepts.Add(kp.Key.Substring(prefix_concept.Length, kp.Key.Length - prefix_concept.Length));
        }
      }
    }
    return concepts;
  }

  //C:\Users\lovehina\AppData\LocalLow\ab\tshatebote
  static public string getSortListFilePath()
  {
    return HalperNatives.getDataPath() + "/sort.txt";
  }

}

/// <summary>
/// some kind of structure to keep data about a phrase
/// encapsulate user input and sort informations
/// </summary>
public class Phrase
{
  InputParser parser;

  public string phraseRef;
  public List<Word> words;
  public List<Pattern> patterns;

  public Phrase(string phrase, InputParser parser)
  {
    this.parser = parser;

    phraseRef = phrase.lowerFirstLetter(); // remove majuscule

    Debug.Log("> " + phraseRef);

    words = parseWords(phraseRef, parser);

    Debug.Log("  L phrase has " + words.Count + " words");

    //patterns = new List<KeyValuePair<string, string[]>>();
    patterns = parser.getMatchingPatterns(this);

    Debug.Log("  L phrase has " + patterns.Count + " matching patterns");
  }

  public Pattern getBestPattern()
  {
    if(patterns.Count <= 0)
    {
      Debug.LogWarning("  L phrase has no matching pattern");
      return null;
    }

    return patterns[0];
  }

  /// <summary>
  /// décompose la phrase en mots
  /// </summary>
  /// <param name="phrase"></param>
  /// <returns></returns>
  public List<Word> parseWords(string phrase, InputParser parser)
  {
    List<Word> tmp = new List<Word>();
    string[] words = phrase.Split(' ');
    for (int i = 0; i < words.Length; i++)
    {
      Word w = new Word(words[i], i, parser);
      tmp.Add(w);
    }
    return tmp;
  }

}

public class Word
{
  public string word;
  public int index;
  public List<string> concepts;

  public Word(string word, int idx, InputParser parser)
  {
    this.word = word;
    this.index = idx;

    concepts = parser.getMatchingConcepts(this.word);
    Debug.Log("    L word '"+this.word+"' has " + concepts.Count + " concepts");
  }
}

public class Pattern
{
  /// <summary>
  /// ([me][love][meat])
  /// -I like meat too!
  /// -I really don't enjoy eating meat
  /// </summary>
  public string patternRef; // ([me][love][meat])
  public List<string> patternParts; //  me,love,meat
  public List<string> answers;

  public Pattern(string pattern)
  {
    patternRef = pattern.Substring(1, pattern.Length-2); // remove ()
    
    //Debug.Log("solving pattern concent : " + patternRef);

    patternParts = new List<string>();

    string[] splitted = patternRef.Split(']');
    Debug.Log("creating new pattern " + patternRef + " , splitted in " + splitted.Length);

    for (int i = 0; i < splitted.Length; i++)
    {
      string split = splitted[i];

      if (split.Length <= 0) continue;

      //remove [] IF needed
      if (split[0] == '[') split = split.Substring(1, split.Length - 1);
      if (split[split.Length - 1] == ']') split = split.Substring(0, split.Length - 1);

      Debug.Log("  L pattern adding word/concept : " + split);
      if(split.Length > 0)
      {
        patternParts.Add(split);
      }
    }

    Debug.Log("  L pattern has " + patternParts.Count + " parts");
  }

  /// <summary>
  /// i love stuff ?== [me][love][stuff]
  /// </summary>
  /// <param name="phrase"></param>
  /// <returns></returns>
  public bool isPhraseMatching(Phrase phrase)
  {
    bool hasMatchingConcept = false;
    int count = 0;

    Debug.Log("solving if phrase match pattern : " + patternRef);

    //si tout les concepts sont dans la phrase
    foreach(Word w in phrase.words) // love
    {
      //at least one concept of word is in pattern parts
      for (int j = 0; j < w.concepts.Count; j++)
      {
        if (patternParts.IndexOf(w.concepts[j]) > -1)
        {
          hasMatchingConcept = true;
        }
      }
  
      if (!hasMatchingConcept)
      {
        Debug.Log("  L pattern has no parts that match word '" + w.word + "' concepts (count ? " + w.concepts.Count+")");
        return false;
      }
      else
      {
        count++;
      }
    }

    //pas assez de mots qui match le pattern
    if(count < patternParts.Count)
    {
      Debug.Log("  L not enought words ("+count+"/"+patternParts.Count+") match parts of pattern");
      return false;
    }

    return true;
  }

  public string getRandomAnswer()
  {
    return answers[Random.Range(0, answers.Count)];
  }
}