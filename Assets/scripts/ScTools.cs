﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScTools : ScreenObject {

  AppManager am;

  public bool forceDisconnect = false;
  public bool connected = false;

  public float anglePerSecond = 360f;
  //public float sinScale = 1f;

  public Image loading;

  public Image cloud;
  public Image upload;
  public Image content;

  public Sprite[] connectedStatusIcons;

  protected override void setup()
  {
    base.setup();

    am = qh.gc<AppManager>();

    //cloud.enabled = false;

#if !UNITY_EDITOR
    forceDisconnect = false;
#endif

    if (forceDisconnect)
    {
      connected = false;
    }
    else
    {
      InvokeRepeating("ping", 1f, 10f);
    }
    
    show();
  }

  public override void show()
  {
    base.show();

    updateVisual();
  }

  protected override void updateVisible()
  {
    base.updateVisible();

    if (Input.GetKeyUp(KeyCode.UpArrow)) ping();

    loading.enabled = am.isBotThinking();

    if (loading.enabled)
    {
      loading.transform.Rotate(Vector3.forward, -anglePerSecond * (Time.deltaTime * Mathf.Abs(Mathf.Sin(Time.time * 2f))));
    }

    upload.enabled = am.isUploading();
  }

  public void onLocalContent()
  {
    content.enabled = am.ip.hasLocalContent();
  }

  void updateVisual()
  {
    
    cloud.sprite = connected ? connectedStatusIcons[0] : connectedStatusIcons[1];
    //cloud.enabled = connected;
  }

  void ping()
  {
    Debug.Log("ping?");
    WebBridge.get().ping(onPingComplete);
  }

  void onPingComplete(bool connected)
  {
    this.connected = connected;

    if (connected) Debug.Log("pong!");
    else Debug.Log("timeout!");

    if (connected)
    {
      am.checkSendOnline(); // send data online if possible
    }
    
    updateVisual();
    //WebBridge.get().ping(onPingComplete);
  }
}
