﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// https://docs.unity3d.com/ScriptReference/UI.InputField.html
/// </summary>

public class UiDialog : MonoBehaviour {

  public float lineMoveVerticalSpeed = 10f;
  public int maxLines = 30;

  AppManager am;

  public float loadingAnglePerSecond = 360f;
  //public float sinScale = 1f;

  public Image loading;

  public Canvas canvas;
  public RectTransform chatPivot;
  public RectTransform inputPivot;

  public RectTransform lineObjectOwner;
  public RectTransform lineObjectOther;

  public InputField iField;

  public Action<string> onInput;

  protected List<UiDialogLine> lines;
  protected bool focused = false;

  private void Start()
  {
    lines = new List<UiDialogLine>();

    am = qh.gc<AppManager>();

    //lineObject = chatPivot.transform.GetChild(0).GetComponent<RectTransform>();
    lineObjectOwner.gameObject.SetActive(false);
    lineObjectOther.gameObject.SetActive(false);

    //iField.lineType = InputField.LineType.MultiLineSubmit;
    
    iField.onEndEdit.AddListener(delegate (string val)
    {
      //remove last newline
      if (val[val.Length - 1] == '\n') val = val.Substring(0, val.Length - 1);

      Debug.Log("event onEndEdit : " + val);

      if (val.Length <= 0) return;

      //Debug.Log("end editing : " + val);

      injectDialog(val, true);

      //empty field after sending
      iField.text = "";

      if(onInput != null) onInput(val);

      //iField.selectionFocusPosition = 0;
    });
    
    /*
    iField.onValidateInput += delegate (string input, int charIndex, char addedChar)
    {
      return addedChar;
    };
    */
  }

  public void reset()
  {
    for (int i = 0; i < lines.Count; i++)
    {
      lines[i].destroy();
    }
    lines.Clear();
    
  }

  public void injectDialog(string ct, bool owner)
  {
    UiDialogLine line = new UiDialogLine(this, owner ? lineObjectOwner : lineObjectOther);

    line.setLine(ct);

    addLineAfterOther(line, getLastDialog());
  }

  public UiDialogLine getLastDialog()
  {
    if (lines.Count <= 0) return null;
    return lines[lines.Count - 1];
  }

  protected void setInputFieldFocus()
  {
    //already focused
    if (iField.isFocused) return;

    // give focus
    EventSystem.current.SetSelectedGameObject(iField.gameObject, null);
    iField.OnPointerClick(new PointerEventData(EventSystem.current));

    //iField.text = "";
    
  }

  void Update()
  {

    if(iField.isFocused && !focused)
    {
      iField.text = "";
      focused = true;
    }else if(!iField.isFocused && focused)
    {
      focused = false;
    }

    if (Input.GetKeyUp(KeyCode.Return))
    {
      Debug.Log("returned : " + iField.isFocused);
      if (!iField.isFocused) setInputFieldFocus();
      //else act_submit();
    }

    if (Input.GetKeyUp(KeyCode.RightControl)) injectDialog("~ping", true);
    if (Input.GetKeyUp(KeyCode.RightShift)) injectDialog("~pong", false);

    update_Thinking();
    
    UiDialogLine line = lines.Count > 0 ? lines[0] : null;
    float sizeHeight = line != null ? getLineHeight(line) + line.rt.getHeight() : 0f;

    //setup scrollview size
    chatPivot.setHeight(sizeHeight);
    
    if (lines.Count > 0)
    {

      //keep line at the right position
      //for (int i = 0; i < lines.Count; i++)
      for (int i = lines.Count-1; i >= 0; i--)
      {
        line = lines[i];

        line.rt.anchoredPosition = Vector3.MoveTowards(line.rt.anchoredPosition,
          new Vector3(0f, getLineHeight(line), 0f), lineMoveVerticalSpeed * Time.deltaTime);

        if (line.group.alpha < 1f) line.group.alpha = Mathf.MoveTowards(line.group.alpha, 1f, Time.deltaTime * 1f);

        line.rt.name = "line#" + i;
      }
    }
    
  }

  protected float getLineHeight(UiDialogLine line)
  {
    for (int i = 0; i < lines.Count; i++)
    {
      if(lines[i] == line)
      {
        UiDialogLine under = null;
        if (i < lines.Count - 1) under = lines[i + 1];

        if (under != null) return under.rt.anchoredPosition.y + under.rt.getHeight();
        else return 0f;
      }
    }

    return 0f;
  }

  protected void update_Thinking()
  {

    loading.enabled = am.isBotThinking();
    if (loading.enabled)
    {
      loading.transform.Rotate(Vector3.forward, -loadingAnglePerSecond * (Time.deltaTime * Mathf.Abs(Mathf.Sin(Time.time * 2f))));
    }


  }

  public void addLineAfterOther(UiDialogLine line, UiDialogLine parent)
  {
    if(parent == null)
    {
      RectTransform mask = line.rt.parent.GetComponent<RectTransform>();

      Vector2 pos = mask.getBottomCenterPosition();
      line.rt.anchoredPosition = pos + (Vector2.down * line.rt.getHeight());

      lines.Add(line);
      //Debug.Log("added first line");
    }
    else
    {

      int parentIdx = lines.IndexOf(parent);

      Vector2 vec = parent.rt.anchoredPosition;
      vec.y -= parent.rt.getHeight();
      line.rt.anchoredPosition = vec;

      //Debug.Log(parentIdx + " / " + lines.Count);

      //if parent is last -> add to list instead of insert
      if (parentIdx == lines.Count - 1)
      {
        lines.Add(line);
        //Debug.Log("added end line");
      }
      else
      {
        lines.Insert(parentIdx, line);
        //Debug.Log("added line at "+parentIdx);
      }
    }

    while(lines.Count > maxLines)
    {
      lines[0].destroy();
      lines.RemoveAt(0);
    }
  }

  public void act_submit()
  {
    Debug.Log("submit");
    //iField.selectionFocusPosition = 0;

    iField.OnDeselect(new BaseEventData(EventSystem.current));
    iField.OnSubmit(new BaseEventData(EventSystem.current));
  }
}

public class UiDialogLine
{
  protected UiDialog uiDiag;
  public RectTransform rt;
  public Text date;
  public Text txt;
  public CanvasGroup group;

  public UiDialogLine(UiDialog uiDiag, RectTransform lineRef)
  {
    this.uiDiag = uiDiag;

    GameObject obj = (GameObject)GameObject.Instantiate(lineRef.gameObject);
    obj.gameObject.SetActive(true);

    rt = obj.GetComponent<RectTransform>();

    rt.transform.SetParent(uiDiag.chatPivot.transform, true);
    //rt.transform.localPosition = Vector3.one;
    rt.transform.localScale = Vector3.one;

    date = HalperComponentsGenerics.getComponentContext<Text>(rt.transform, "timestamp");
    date.text = "";

    txt = HalperComponentsGenerics.getComponentContext<Text>(rt.transform, "label");
    txt.text = "";

    group = rt.GetComponent<CanvasGroup>();
    group.alpha = 0f;
  }
  
  public void setLine(string newContent)
  {
    date.text = HalperNatives.getNowHourMin();
    txt.text = newContent;

    rt.setHeight(newContent.Split('\n').Length * rt.sizeDelta.y);
  }

  public void destroy()
  {
    GameObject.DestroyImmediate(rt.gameObject);
    rt = null;
    txt = null;
  }
  
}